#pragma once

#include <functional>

#include <QThread>
#include <QMetaObject>

namespace utils
{
template <typename... Args>
static void invoke(QObject* receiver, std::function<void(Args...)> callback, const Args&... args)
{
    if (receiver->thread() != QThread::currentThread())
    {
        QMetaObject::invokeMethod(receiver, [=]()
        {
            callback(args...);
        });
    }
    else
    {
        callback(args...);
    }
}

} // namespace utils
