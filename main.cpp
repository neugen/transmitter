#include <QTimer>
#include <QCoreApplication>

#include "Test.h"

int main(int argc, char *argv[])
{
    QCoreApplication a(argc, argv);

    auto* task = new Task(&a);
    QTimer::singleShot(0, task, SLOT(run()));

    return a.exec();
}
