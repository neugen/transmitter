#pragma once

#include <QObject>

class Task : public QObject
{
    Q_OBJECT

public:
    explicit Task(QObject* parent)
        : QObject(parent) {}

public slots:
    void run();

};
