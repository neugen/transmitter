#include "Test.h"

#include <thread>

#include <QDebug>
#include <QEventLoop>

#include "utils.h"

namespace
{
    using CallbackType = std::function<void(QString)>;

    static CallbackType g_fn = [](QString param)
    {
        qDebug() << param;
    };

    QObject* g_context = nullptr;

} // anonymous namespace


void testSingleThreaded()
{
    QObject context;
    utils::invoke(&context, g_fn, QString("main to main"));
}

void testMultiThreaded_1()
{
    g_context = new QObject();

    std::thread([&]
    {
        utils::invoke(g_context, g_fn, QString("custom to main"));
        g_context = nullptr;

    }).join();
}

void testMultiThreaded_2()
{
    QEventLoop* loop = nullptr;

    std::thread([&loop]
    {
        loop = new QEventLoop;
        g_context = new QObject();
        loop->exec();

    }).detach();

    std::thread([&loop]
    {
        std::this_thread::sleep_for(std::chrono::seconds(2));

        utils::invoke(g_context, g_fn, QString("custom to custom"));
        g_context = nullptr;

        loop->quit();
        loop->deleteLater();

    }).detach();
}

void Task::run()
{
    testSingleThreaded();
    testMultiThreaded_1();
    testMultiThreaded_2();
}
